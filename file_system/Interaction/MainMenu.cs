﻿using file_management.Services;
using System;

namespace file_management.Interaction
{
	class MainMenu
	{
		enum Options
		{
			// Setting index of enums, else the index will start at 0
			LIST_ALL_FILES = 1,
			LIST_ALL_JPG = 2,
			LIST_ALL_JPEG = 3,
			LIST_ALL_PNG = 4,
			LIST_ALL_JFIF = 5,
			LIST_ALL_TXT = 6,
			DRACULA_TXT_FILEINFO = 7,
			SEARCH_FOR_WORD = 8,
			EXIT = 9
		}
		public static bool Menu()
        {
            CreateMenu();
            return ExecuteMenuOption();
        }

        private static void CreateMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) List all files");
            Console.WriteLine("2) List all jpg");
            Console.WriteLine("3) List all jpeg");
            Console.WriteLine("4) List all png");
            Console.WriteLine("5) List all jfif");
            Console.WriteLine("6) List all txt");
            Console.WriteLine("7) Get Dracula.txt info");
            Console.WriteLine("8) Search Dracula.txt for word");
            Console.WriteLine("9) Exit");
            Console.Write("\r\nSelect an option: ");
        }

        private static bool ExecuteMenuOption()
		{

			//parses the input into the Enum options 
			if (Enum.TryParse<Options>(Console.ReadLine(), ignoreCase: true, out var option))
			{

				switch (option)
				{
					case Options.LIST_ALL_FILES:

						FileService.ListAllFiles();
						DisplayResult();
						return true;

					case Options.LIST_ALL_JPG:

						FileService.listFilesByExtension("jpg");
						DisplayResult();
						return true;

					case Options.LIST_ALL_JPEG:

						FileService.listFilesByExtension("jpeg");
						DisplayResult();
						return true;

					case Options.LIST_ALL_PNG:

						FileService.listFilesByExtension("png");
						DisplayResult();
						return true;

					case Options.LIST_ALL_JFIF:

						FileService.listFilesByExtension("jfif");
						DisplayResult();
						return true;

					case Options.LIST_ALL_TXT:

						FileService.listFilesByExtension("txt");
						DisplayResult();
						return true;


					case Options.DRACULA_TXT_FILEINFO:

						FileService.getFileInfo("Dracula.txt");
						DisplayResult();
						return true;

					case Options.SEARCH_FOR_WORD:

						Console.WriteLine("Enter word to search for");
						string searchWord = Console.ReadLine();
						FileManipulation.searchFileForWord(searchWord, "Dracula.txt");
						DisplayResult();
						return true;

					case Options.EXIT:

						Console.WriteLine("EXIT");
						return false;

					default:
						// if user types in none of the valid it options
						Console.WriteLine("Invalid option");
						DisplayResult();
						return true;
				}
			}
			return true;
		}



		private static void DisplayResult()
		{
			//Since console is cleared each time, we readline again so the results stays visible
			Console.Write("\r\nPress Enter to return to Main Menu");
			Console.ReadLine();
		}
	}
}
