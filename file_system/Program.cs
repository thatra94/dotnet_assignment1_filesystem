﻿using file_management.Interaction;

namespace file_system
{
    class Program
    {
        static void Main(string[] args)
        {

            //as long as user doesn't enter the option to exit the menu will stay open
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu.Menu();
            }
        }
    }
}

