﻿using System;
using System.IO;

namespace file_management.Services
{
    class LoggingService
    {
        public static void writeLogging(DateTime processStart, DateTime processEnd, string functionExectued)
        {
            // Subtract process end to processtart to get time taken for the process
            TimeSpan processTime = processEnd.Subtract(processStart);

            // Cast processtime into int and get miliseconds
            int processInMili = (int)processTime.TotalMilliseconds; 
            Console.WriteLine($"{ processStart}: {functionExectued} took {processInMili} ms to complete");

            // Set to true to not overwrite existing log
            using (StreamWriter streamWriter = new StreamWriter(@"../../../Resources/Logging.txt", true)) 
            {
                streamWriter.WriteLine($"{ processStart}: {functionExectued} took {processInMili} ms to complete");
            }
        }

    }
} 
