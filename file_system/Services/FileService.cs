﻿using System;
using System.IO;

namespace file_management.Services
{
    class FileService
    {
        public static void ListAllFiles()
        {
            DateTime processStart = DateTime.Now;

            try
            {

                string[] fileEntries = Directory.GetFiles(@"../../../Resources");
                Console.WriteLine($"The number of files in directory {fileEntries.Length}.");
                foreach (string fileName in fileEntries)
                {
                    Console.WriteLine(fileName);
                }
                DateTime processEnd = DateTime.Now;
                LoggingService.writeLogging(processStart, processEnd, "ListAllFiles");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: ", e.ToString());
            }
        }

        public static void listFilesByExtension(string extension)
        {
            DateTime processStart = DateTime.Now;

            try
            {
                string[] fileEntries = Directory.GetFiles(@"../../../Resources", "*." + extension);
                Console.WriteLine($"The number of files in directory : {fileEntries.Length}");
                foreach (string fileName in fileEntries)
                {
                    Console.WriteLine(fileName);
                }
                DateTime processEnd = DateTime.Now;
                LoggingService.writeLogging(processStart, processEnd, $"listFilesByExtension {extension}");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: ", e.ToString());
            }
        }
        public static void getFileInfo(string fileName)
        {
            DateTime processStart = DateTime.Now;
            try
            {
                string filePath = $@"../../../Resources/{fileName}";
                FileInfo fileInfo = new FileInfo(filePath);

                if (File.Exists(filePath))
                {
                    Console.WriteLine(filePath);
                    // get file size on disk
                    Console.WriteLine($"File size: {fileInfo.Length} bytes");
                    getFileLines(filePath);

                }
                DateTime processEnd = DateTime.Now;
                LoggingService.writeLogging(processStart, processEnd, $"get {fileName} info ");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: ", e.ToString());
            }
     

        }

        public static void getFileLines(string filePath)
        {
            // reads all lines and counts them
            using (StreamReader file = new StreamReader(filePath))
            {
                int counter = 0;

                while ((file.ReadLine()) != null)
                {
                    counter++;
                }
                Console.WriteLine($"File has {counter} lines.");
            }
        }

    }
}
