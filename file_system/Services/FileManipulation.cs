﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace file_management.Services
{
    class FileManipulation
    {
        public static int searchFileForWord(string word, string fileName)
        {
            DateTime processStart = DateTime.Now;
            try
            {
                int wordOccurence = 0;
                string filePath = $@"../../../Resources/{fileName}";
                using (StreamReader file = new StreamReader(filePath))
                {
                    while (!file.EndOfStream)
                    {
                        // Counts the number of times we find the search word with ignoring of case
                        string line = file.ReadLine();
                        Regex wordToFind = new Regex($"{word}", RegexOptions.IgnoreCase);
                        foreach (Match matchedWord in wordToFind.Matches(line))
                        {
                            wordOccurence++;
                        }
                    }
                }
                DateTime processEnd = DateTime.Now;
                LoggingService.writeLogging(processStart, processEnd, $"{word} was found {wordOccurence} times and");
                return wordOccurence;
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: ", e.ToString());
            }
           
            return 0;
        }
    }
}

