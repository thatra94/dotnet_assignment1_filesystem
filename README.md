# Dotnet_assignment1_filesystem

This is an assignment for NOROFF dotnet program. 
It is a C# console program that can list files in the resource folder, search a text file for specific words and writes a log to a logging file. 

To start the program run it in an editor like Visual Studio

## Assignment Requirements
- [x]  Placing resources in the appropriate folder
- [x]  Accessing the resources according to specifications
- [x]  Gather and display information about the resources
- [x]  Proper user interaction through menus
- [x]  Log all actions to a file for review 
- [x]  Proper commenting
- [x]  Logical grouping

## User interaction and input 

- [x] Make a menu that persists after taking a choice
- [x] Option to exit the program from here

## File manipulation

- [x] Fileservice that gathers information about files
- [x] Method that lists all the file names in the resources directory
- [x] Method to get files by their extension

**Dracula.txt**

- [x] Get file name
- [x] Get file size
- [x] Count number of lines the file has
- [x] Able to search for a specific word
- [x] Show how many times the word is found in the file

## Logging

- [x] Create logging service that write to a file
- [x] Log results from the "file manipulation" functionality"
- [x] Display message to console
- [x] Duration of each function calls must be logged
